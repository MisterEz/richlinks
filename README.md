## Richlinks
Richlinks is an experimental library for generating a rich preview or embed from a url. 

Note: Richlinks is not maintained in any respect. Use https://noembed.com/ instead.


Demo: http://rl.nm.tc/demo/


### Building the demo 

```npm install```

```npm run build```

### Running the chat example

Dependencies for python3

```sudo apt install python3-bs4 python3-pil python3-socketio-client python3-aiohttp```

```pip3 install python-oembed```

```pip3 install python-socketio```

then run resources/python/chat.py