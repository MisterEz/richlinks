var gulp       = require('gulp');
var sass       = require('gulp-sass');
var autoprefix = require('gulp-autoprefixer');
var notify     = require("gulp-notify");

var config = {
	sassPath: './resources/sass',
};

gulp.task('sass', () => {
    return gulp
        .src(config.sassPath + '/styles.scss')
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: [
                './node_modules/bootstrap-sass/assets/stylesheets/'
            ]
        }))
        .pipe(autoprefix({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .on("error", notify.onError(function (error) {
			return "Error: " + error.message;
		}))
        .pipe(gulp.dest("./public/css"));
});